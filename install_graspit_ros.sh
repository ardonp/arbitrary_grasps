#! /bin/sh
#GRASPIT SETUP 

#Download and install graspit requirements
sudo apt-get install libqt4-dev libqt4-opengl-dev libqt4-sql-psql libcoin80-dev libsoqt4-dev libblas-dev liblapack-dev libqhull-dev

#Download and install graspit ros pluging 
git clone https://github.com/graspit-simulator/graspit.git
cd graspit
mkdir build
cd build
cmake ..
make -j16
sudo make install

#Export the plugin environment variables or copy them to your /.bashrc file 
echo 'export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH' >> ~/.bashrc
echo 'export GRASPIT=~/.graspit' >> ~/.bashrc
surce ~/.bashrc