#! /usr/bin/env python

#https://stackoverflow.com/questions/287871/how-to-print-colored-text-in-terminal-in-python
styles = {
    "HEADER": '\033[95m', # magenta
    "OKBLUE": '\033[94m',
    "OKGREEN": '\033[92m',
    "WARNING": '\033[93m', # yellow
    "FAIL": '\033[91m',
    "BOLD": '\033[1m',
    "UNDERLINE": '\033[4m',
    "HIGHLIGHT": '\033[104m'
}
ENDC = '\033[0m'

def cout(text, style_list=''):
    if not isinstance(style_list, list):
        style_list = [style_list]

    format = ''
    for style in style_list:
        format += styles.get(style, '')

    print('{}{}{}').format(format, text, ENDC)
