#! /usr/bin/env python

import rospy
import numpy as np
import geometry_msgs.msg

def arrayToPoseMsg(pose):
    return geometry_msgs.msg.Pose(geometry_msgs.msg.Point(*pose[0:3]), geometry_msgs.msg.Quaternion(*pose[3:7]))

def arrayToPoseStampedMsg(pose, source_frame):
    msg = geometry_msgs.msg.PoseStamped()
    msg.header.frame_id = source_frame
    msg.header.stamp = rospy.Time.now()
    msg.pose = arrayToPoseMsg(pose)
    return msg

def poseMsgToArray(msg):
    return np.asarray([msg.position.x, msg.position.y, msg.position.z, msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w])

def poseStampedMsgToArray(msg):
    return poseMsgToArray(msg.pose)
