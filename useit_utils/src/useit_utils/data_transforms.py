#! /usr/bin/env python

import numpy as np
import tf

def poseToTransform(t, q):
    return np.dot(tf.transformations.translation_matrix(t), tf.transformations.quaternion_matrix(q))

def transformToPose(h):
    return tf.transformations.translation_from_matrix(h), tf.transformations.quaternion_from_matrix(h)

def invertTransform(h):
    return tf.transformations.inverse_matrix(h)

def invertPose(trans, rot):
    return transformToPose(invertTransform(poseToTransform(trans, rot)))

def transformPose(h, pose):
    txpose = np.dot(h, poseToTransform(pose[0:3], pose[3:7]))
    return transformToPose(txpose)

def transformTrajectory(h, trajectory):
    path = []
    for pose in trajectory:
        path.append(np.concatenate((transformPose(h, pose)), axis=0))
    return np.asarray(path)

def computeLocalFrame(start, goal):
    # create local axis
    # x-axis points to goal
    # z and y axis are set perpendicular to x-axis
    x_axis = goal[0:3] - start[0:3]
    x_axis /= np.linalg.norm(x_axis)

    z_axis = np.array([0.0, 0.0, 1.0])
    z_axis -= z_axis.dot(x_axis) * x_axis
    z_axis /= np.linalg.norm(z_axis)

    y_axis = np.cross(z_axis, x_axis)

    # define rotation matrix from axis
    M = np.eye(4, dtype=float) #rotation matrix
    M[0:3,0] = x_axis
    M[0:3,1] = y_axis
    M[0:3,2] = z_axis

    return start[0:3], tf.transformations.quaternion_from_matrix(M)
