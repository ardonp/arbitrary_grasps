#! /usr/bin/env python

from std_msgs.msg import ColorRGBA
from visualization_msgs.msg import Marker, MarkerArray
from geometry_msgs.msg import Point, Pose, PoseArray, PoseStamped

import msg_conversions as mc

def plotTrajectoryAsPoints(trajectory, frame, publisher, ns, colour):
    marker = Marker()
    marker.header.frame_id = frame
    marker.ns = ns
    marker.type = marker.LINE_STRIP
    marker.action = marker.ADD
    marker.scale.x = 0.005
    marker.color = ColorRGBA(colour[0], colour[1], colour[2], 1.0)
    marker.pose.orientation.w = 1.0

    for pose in trajectory:
        marker.points.append(Point(*pose[0:3]))

    publisher.publish(marker)

def plotSpheres(trajectory, frame, publisher, ns, colour):
    marker_array = MarkerArray()

    for i, pose in enumerate(trajectory):
        marker = Marker()
        marker.header.frame_id = frame
        marker.ns = ns + str(i)
        marker.type = marker.SPHERE
        marker.action = marker.ADD
        marker.scale.x = 0.02
        marker.scale.y = 0.02
        marker.scale.z = 0.02
        marker.color = ColorRGBA(colour[0], colour[1], colour[2], 1.0)
        marker.pose.position = Point(*pose[0:3])
        marker.pose.orientation.w = 1.0

        marker_array.markers.append(marker)
    publisher.publish(marker_array)

def plotTrajectoryAsFrames(trajectory, frame, publisher):
    marker = PoseArray()
    marker.header.frame_id = frame

    for pose in trajectory:
        marker.poses.append(mc.arrayToPoseMsg(pose))

    publisher.publish(marker)

def plotPose(pose, frame, publisher):
    marker = PoseStamped()
    marker.header.frame_id = frame
    marker.pose = mc.arrayToPoseMsg(pose)
    publisher.publish(marker)
