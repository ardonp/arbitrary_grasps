# GRASPIT SETUP

Package adapted from the original [graspit](https://github.com/graspit-simulator/graspit)

## Installation
Create workspace
```
mkdir -p ~/ws_grasp_baselines/src
cd ~/ws_grasp_baselines/src
```

Download this repository
```
cd ~/ws_grasp_baselines/src
git clone https://ardonpaola@bitbucket.org/ardonp/arbitrary_grasps.git
```

Install graspit ros plugin
```
cd ~/ws_grasp_baselines/src/arbitrary_grasps
bash install_graspit_ros.sh
```

Build the catkin workspace:
```
cd ~/ws_grasp_baselines
catkin build
```

Source the catkin workspace:
```
echo 'source ~/ws_grasp_baselines/devel/setup.bash' >> ~/.bashrc
```

## Usage
- To add objects for grasp detection, they need to be added to the graspit_ros_plugin. The 3D model in
`~/graspit/models/objects` and with the surrounding environment as worlds in `~/graspit/worlds`

- Some object models can be found [here](https://tiiuae-my.sharepoint.com/:f:/g/personal/paola_ardon_tii_ae/EuCyUkzKt_1At2RYwg2XJGgBRcxDoibV_Auv0VpB9fgovA?e=9tXHM0)

- To see the demo with sample of arbitrary grasps
```
roslaunch graspit_demo arbitrary_grasps.launch
```

- The resulting grasp poses are saved in `~/graspit/src/data` or `rostopic echo /graspit/planGrasps/feedback`


### Known issues:
- qt boost libraries to have conflict. To fix it adapt the CmakeList.txt in graspit_interface following this [guidelines](https://github.com/graspit-simulator/graspit_interface/pull/38/commits/04aaebb6e47d7e3d965864d0da6bb36d7c2aa276#diff-1e7de1ae2d059d21e1dd75d5812d5a34b0222cef273b7c3a2af62eb747f9d20a)
