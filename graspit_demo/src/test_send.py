import rospy
import time
import roslib; roslib.load_manifest('robotiq_2f_gripper_control')
from robotiq_2f_gripper_control.msg import _Robotiq2FGripper_robot_output  as outputMsg

def open_grip(pub):
    msg = outputMsg.Robotiq2FGripper_robot_output()
    msg.rACT = 1
    msg.rGTO = 1
    msg.rATR = 0
    msg.rPR  = 0
    msg.rSP = 255
    msg.rFR = 150
    print('opening biatch')
    pub.publish(msg)
    time.sleep(4)

def close_grip(pub):
	msg = outputMsg.Robotiq2FGripper_robot_output()
	msg.rACT = 1
	msg.rGTO = 1
	msg.rATR = 0
	msg.rPR  = 255
	msg.rSP = 255
	msg.rFR = 150
	pub.publish(msg)
	time.sleep(4)


if __name__ == "__main__":
	rospy.init_node("moveit_demo")

	pub = rospy.Publisher("Robotiq2FGripperRobotOutput",outputMsg.Robotiq2FGripper_robot_output,queue_size=10)

	while not rospy.is_shutdown():
		open_grip(pub)
		rospy.sleep(0.1)
