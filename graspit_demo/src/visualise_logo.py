#!/usr/bin/env python

import rospy
import rospkg
from visualization_msgs.msg import Marker

def logo():
	rospy.init_node("visualise_logo")
	mesh_pub = rospy.Publisher('grasp_mesh', Marker, queue_size=10)
	r = rospy.Rate(1)

	while (not rospy.is_shutdown()):
		r.sleep()

		marker = Marker()
		marker.header.frame_id = 'base_link' #panda_link0
		marker.ns = "logo"
		marker.type = marker.MESH_RESOURCE
		marker.id = 0
		marker.mesh_resource = "package://graspit_demo/models/mesh/whole_logo.dae"
		marker.action = marker.ADD
		marker.scale.x = 0.25
		marker.scale.y = 0.25
		marker.scale.z = 0.25
		marker.pose.position.x = 0.5
		marker.pose.position.y = 5
		marker.pose.position.z = 0.5
		marker.pose.orientation.x = 0
		marker.pose.orientation.y = 0
		marker.pose.orientation.z = 0
		marker.pose.orientation.w = 0
		marker.mesh_use_embedded_materials = True
		mesh_pub.publish(marker)



if __name__ == "__main__":

	logo()
