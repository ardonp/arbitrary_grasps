#!/usr/bin/env python
import rospy
import graspit_commander
import pickle
import os

world_list = ["bpl_cylinder"]
# world_list = ["cylinder"]
cwd = os.getcwd()
store_data = cwd + '/data/'

def plan_grasps(gc):

	for world in world_list:
		pkl_file = str(store_data) + world + "_arbitrary_grasps.npy"
		with open(pkl_file,"a") as f:
			gc.clearWorld()
			gc.loadWorld(world)
			response = gc.planGrasps()
			grasps = response.grasps
			print type(grasps)

			pickle.dump(grasps,f,pickle.HIGHEST_PROTOCOL)

	return grasps

if __name__ == "__main__":
	rospy.init_node("arbitrary_grasps_demo")
	gc = graspit_commander.GraspitCommander()
	grasps = plan_grasps(gc)
