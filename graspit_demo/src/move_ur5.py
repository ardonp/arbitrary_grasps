import moveit_commander
import utils as gu
import rospy
import roslib; roslib.load_manifest('robotiq_2f_gripper_control')
from robotiq_2f_gripper_control.msg import _Robotiq2FGripper_robot_output  as outputMsg




if __name__ == "__main__":
	rospy.init_node("moveit_demo")
	robot = moveit_commander.RobotCommander()

	pub = rospy.Publisher("Robotiq2FGripperRobotOutput",outputMsg.Robotiq2FGripper_robot_output,queue_size=10)

	while not rospy.is_shutdown():
		gu.open_grip(pub)
		rospy.sleep(0.1)

	# gu.spawn_mug()
	# gu.open_grip(pub)
	#gu.raise_torso(robot)
	# try:
	# 	gu.home_arm()
	# except:
	# 	pass
	# gu.execute_traj(robot)
