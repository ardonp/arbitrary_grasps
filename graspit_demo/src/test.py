import roslib; roslib.load_manifest('robotiq_2f_gripper_control')
import rospy
from std_msgs.msg import String
from robotiq_2f_gripper_control.msg import _Robotiq2FGripper_robot_input  as inputMsg
from robotiq_2f_gripper_control.msg import _Robotiq2FGripper_robot_output  as outputMsg
import time

# node to get status

def printRobotiqStatus(status):
    print robotiq_get_fault_status(status)
    print robotiq_get_activation_status(status)
    print robotiq_get_action_status(status)
    print robotiq_get_movement_status(status)
    print robotiq_get_gripper_status(status)
    print '\n'


def robotiq_listener():
    # rospy.init_node('listenToRobotiqStatus')
    rospy.Subscriber("Robotiq2FGripperRobotInput", inputMsg.Robotiq2FGripper_robot_input, printRobotiqStatus)
    rospy.spin()

# get fault status
def robotiq_get_fault_status(fault_status):
    all_good = False

    fault_status_output = 'ERRORs: '
    # gFLT Fault status returns general error messages that are useful for troubleshooting
    if (fault_status.gFLT == 0x00):
        fault_status_output += ' all good in the hood'
        all_good = True
    if (fault_status.gFLT == 0X05):
        fault_status_output += ' Priority Fault: Action delayed, initialization must be completed prior to action'
        all_good = False
    if (fault_status.gFLT == 0x07):
        fault_status_output += ' Priority Fault: The activation bit must be set prior to action\n'
        all_good = False
    if (fault_status.gFLT == 0x09):
        fault_status_output += ' Minor Fault: The communication chip is not ready (may be booting)\n'
        all_good = False
    if (fault_status.gFLT == 0x0B):
        fault_status_output += ' Minor Fault: Automatic release in progress\n'
        all_good = False
    if (fault_status.gFLT == 0x0E):
        fault_status_output += ' Major Fault: Overcurrent protection triggered\n'
        all_good = False
    if (fault_status.gFLT == 0x0F):
        fault_status_output += ' Major Fault: Automatic release completed\n'
        all_good = False
    return(fault_status_output)

# get activation status
def robotiq_get_activation_status(activation_status):
    activation_status_output = 'ACTIVE: '
    active_gripper = False
    if(activation_status.gACT == 0):
        activation_status_output += 'Gripper is not active'
        active_gripper = False
    if(activation_status.gACT == 1):
        activation_status_output += 'Gripper is activated'
        active_gripper = True
    # print 'gripper activation status: '
    return(activation_status_output)
    
# get action status
def robotiq_get_action_status(action_status):
    action_status_output = 'ACTION: '
    if(action_status.gGTO == 0):
        action_status_output += 'Standby (or performing activation/automatic release)'
    if(action_status.gGTO == 1):
        action_status_output += 'Ready to receive commands'
    # print 'gripper activation status: '
    return(action_status_output)

# get motion status
def robotiq_get_movement_status(movement_status):
    movement_status_output = 'MOTION: '
    if(movement_status.gOBJ == 0):
        movement_status_output += 'Fingers are in motion (gGTO = 1)'
    if(movement_status.gOBJ == 1):
        movement_status_output += 'Fingers stopped due to contact while opening'
    if(movement_status.gOBJ == 2):
        movement_status_output += 'Fingers stopped due to contact while closing'
    if(movement_status.gOBJ == 3):
        movement_status_output += 'Fingers reached desired position'
    # print 'gripper activation status: '
    return(movement_status_output)

# get gripper status
def robotiq_get_gripper_status(gripper_status):
    gripper_status_output = 'Gripper: '
    if(gripper_status.gSTA == 0):
        gripper_status_output += 'Gripper is in reset ( or automatic release ) state. see Fault Status if Gripper is activated)'
    if(gripper_status.gSTA == 1):
        gripper_status_output += 'Activation in progress'
    if(gripper_status.gSTA == 2):
        gripper_status_output += 'Not used'
    if(gripper_status.gSTA == 3):
        gripper_status_output += 'Activation is completed'
    # print 'gripper activation status: '
    return(gripper_status_output)

def open_grip(pub):
    msg = outputMsg.Robotiq2FGripper_robot_output()
    msg.rACT = 1
    msg.rGTO = 1
    msg.rATR = 0
    msg.rPR  = 0
    msg.rSP = 255
    msg.rFR = 150
    print('opening biatch')
    pub.publish(msg)
    time.sleep(4)

def close_grip(pub):
	msg = outputMsg.Robotiq2FGripper_robot_output()
	msg.rACT = 1
	msg.rGTO = 1
	msg.rATR = 0
	msg.rPR  = 255
	msg.rSP = 255
	msg.rFR = 150
	pub.publish(msg)
	time.sleep(4)

# def robotiq_get_active_status(active_stat)
if __name__ == '__main__':
    rospy.init_node("moveit_demo")
    robotiq_listener()
    pub = rospy.Publisher("Robotiq2FGripperRobotOutput",outputMsg.Robotiq2FGripper_robot_output,queue_size=10)
    rospy.sleep(0.1)
    close_grip(pub)
    rospy.sleep(0.5)
    open_grip(pub)

