import os
import graspit_commander
import rospy
import time
import moveit_commander
from geometry_msgs.msg import PoseStamped
from moveit_msgs.srv import GetPositionIKRequest, GetPositionIK
import pr2_controllers_msgs.msg
import copy

import tf
import tf_conversions
import pickle


world_list = ["useit_bottle"]

def plan_grasps(gc):


	#world_list = ["useit_RedCup"]#"useit_bottle","useit_bowl", "useit_cooking_spoon", "useit_RedCup"]
	#world_grasps = {}


	for world in world_list:

			for i in range(5):
				with open (str(i)+"_"+world+"_grasps"+".npy","a") as f:
					print(world)
					gc.clearWorld()
					gc.loadWorld(world)

					response = gc.planGrasps()
					grasps = response.grasps


					pickle.dump(grasps,f,pickle.HIGHEST_PROTOCOL)

		#world_grasps["grasps%s" %world] =  grasps


		#grasps = response.grasps

	#print(world_grasps)
	#return world_grasps


def raise_torso(robot):
    tor = moveit_commander.MoveGroupCommander("torso")
    tor.set_pose_reference_frame("base_link")
    tor.go([0.2])

def home_right_arm(robot):

    mgc = moveit_commander.MoveGroupCommander("right_arm")
    jv = mgc.get_current_joint_values()
    jv[0] = -1

    mgc.set_joint_value_target(jv)
    p = mgc.plan()
    mgc.execute(p)


def is_reachable(robot,object_pose):
	for world in world_list:
		for i in range(5):
			with open(str(i)+"_"+world+"_grasps"+".npy","r") as f:
				grasps = pickle.load(f)

				for idx,grasp in enumerate(grasps):
					g = grasps[idx]
					#print idx

					#print ("GRASPS FROM FILE",str(i)+"_"+world+"_grasps")

					group = robot.get_group('right_arm')
					poseStamped = group.get_current_pose()
					pose = poseStamped.pose

					pre_grasp_to_final_offset = 0.03 #0.05

					pre_grasp_pose = PoseStamped()
					pre_grasp_pose.header.frame_id = "base_link"
					pre_grasp_pose.pose = copy.deepcopy(g.pose)

					#offsets
					pre_grasp_pose.pose.position.z += pre_grasp_to_final_offset

					pre_grasp_pose.pose.position.x += object_pose[0]
					pre_grasp_pose.pose.position.y += object_pose[1]
					pre_grasp_pose.pose.position.z += object_pose[2]

					req = GetPositionIKRequest()
					req.ik_request.group_name = "right_arm"
					req.ik_request.robot_state = robot.get_current_state()
					req.ik_request.avoid_collisions = True
					#req.ik_request.ik_link_name = arm_move_group.get_end_effector_link()
					req.ik_request.pose_stamped = pre_grasp_pose
					k = compute_ik_srv(req)
					#print(str(k.error_code))

					if str(k.error_code) == "val: 1":
						#print(g.pose)
						print(pre_grasp_pose)
						return pre_grasp_pose

def execute_traj(grasps,robot,object_pose):
	group = robot.get_group('right_arm')
	poseStamped = group.get_current_pose()
	pose = poseStamped.pose
	# pose.orientation = g.pose.orientation
	# traj = group.plan(pose)
	# group.execute(traj)

	pre_grasp_to_final_offset = 0.05 #0.05

	pre_grasp_pose = copy.deepcopy(grasps.pose)
	pre_grasp_pose.position.z += pre_grasp_to_final_offset

	#offsets
	pre_grasp_pose.position.x += object_pose[0]
	pre_grasp_pose.position.y += object_pose[1]
	pre_grasp_pose.position.z += object_pose[2]


	traj = group.plan(pre_grasp_pose)
	group.execute(traj)

	final_grasp_pose = copy.deepcopy(pre_grasp_pose)
	final_grasp_pose.position.z -= pre_grasp_to_final_offset
	traj = group.plan(final_grasp_pose)
	group.execute(traj)


if __name__ == "__main__":
	rospy.init_node("create_grasps_demo")
	robot = moveit_commander.RobotCommander()
	compute_ik_srv = rospy.ServiceProxy("/compute_ik", GetPositionIK)

	#gc = graspit_commander.GraspitCommander()

	#grasps = plan_grasps(gc)

	object_pose = [0.60514, -0.026986, 0.55]
	reachable_grasp = is_reachable(robot, object_pose) #bottle
	execute_traj(reachable_grasp,robot,object_pose)
