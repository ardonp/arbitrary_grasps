import rospy
import rospkg
from visualization_msgs.msg import Marker
from geometry_msgs.msg import PoseStamped, PoseArray
from tf.transformations import quaternion_from_euler

import tf
import tf_conversions
import pickle

import numpy as np
import math

import useit_utils.rviz_artist as ra
import useit_utils.cool_printer as cp
import useit_utils.msg_conversions as mc

world_list = ["cylinder"]

object_list = ['cylinder']

grasp_graspit_pose_3D = rospy.Publisher("/grasp_graspit_pose", PoseStamped,queue_size=10)
grasp_graspit_pose_3D_oth = rospy.Publisher("/grasp_graspit_pose_oth", PoseStamped,queue_size=10)

def visualise_database():
	#rospy.init_node("visualise_database")
	listener = tf.TransformListener()

	grasps_cylinder_pub = rospy.Publisher('grasps_cylinder', PoseArray, queue_size=10)
	#grasps_RedCup_pub = rospy.Publisher('grasps_RedCup', PoseArray, queue_size=10)
	#grasps_bowl_pub = rospy.Publisher('grasps_bowl', PoseArray, queue_size=10)

	pub_list = [grasps_cylinder_pub]

	mesh_pub = rospy.Publisher('grasp_mesh', Marker, queue_size=10)


	path = rospkg.RosPack().get_path('graspit_demo') + '/src/data/'

	# rospy.sleep(rospy.Duration(3))
	#
	r = rospy.Rate(1)
	while (not rospy.is_shutdown()):
		r.sleep()

		for i, world in enumerate(world_list):

			print(object_list[i])

			listener.waitForTransform('panda_link0', object_list[i], rospy.Time(0), rospy.Duration(1))
			(trans, rot) = listener.lookupTransform('panda_link0', object_list[i], rospy.Time(0)) #panda_link0 and graspit are in the same position now

			grasp_list = []
			# for i in range(5):
			# 	with open(path + str(i)+"_"+world+"_grasps"+".npy","r") as f:
			# with open(path+world+'_grasps.npy',"r") as f:
			with open(path+world+'_arbitrary_grasps.npy',"r") as f:
				grasps = pickle.load(f)

				cp.cout('File {} contains {} grasps'.format(world+'_arbitrary_grasps.npy', len(grasps)), ['WARNING', 'UNDERLINE'])

				for idx, grasp in enumerate(grasps):

					#print grasp.pose
					grasp_pose = mc.poseMsgToArray(grasp.pose)

					#print idx
					#print grasp_pose

					#trans = [0, 0, 0]
					rot = tf.transformations.quaternion_from_euler(1.57, 0, 0) # error correction from graspit to new moveit messages
					#print rot

					pose44 = np.dot(tf.transformations.translation_matrix(grasp_pose[0:3]), tf.transformations.quaternion_matrix(grasp_pose[3:8]))
					mat44 = np.dot(tf.transformations.translation_matrix(trans), tf.transformations.quaternion_matrix(rot))
					txpose = np.dot(mat44, pose44)
					# txpose = np.dot(pose44, mat44)
					pose = np.concatenate((tf.transformations.translation_from_matrix(txpose), tf.transformations.quaternion_from_matrix(txpose)), axis=0)
					#pose = grasp_pose

					## Plottin the PR2 gripper (baseline for graspit generation)
					# marker = Marker()
					# marker.header.frame_id = 'panda_link0'
					# marker.ns = "rotated"
					# marker.type = marker.MESH_RESOURCE
					# marker.id = idx
					# marker.mesh_resource = "package://graspit_demo/models/mesh/whole_gripper.dae"
					# marker.action = marker.ADD
					# marker.scale.x = 1.0
					# marker.scale.y = 1.0
					# marker.scale.z = 1.0
					# marker.pose.position.x = pose[0]
					# marker.pose.position.y = pose[1]
					# marker.pose.position.z = pose[2]
					# marker.pose.orientation.x = pose[3]
					# marker.pose.orientation.y = pose[4]
					# marker.pose.orientation.z = pose[5]
					# marker.pose.orientation.w = pose[6]
					# marker.mesh_use_embedded_materials = True
					# mesh_pub.publish(marker)

					marker = Marker()
					marker.header.frame_id = 'panda_link0'
					marker.ns = "rotated"
					marker.type = marker.MESH_RESOURCE
					marker.id = idx
					marker.mesh_resource = "package://graspit_demo/models/mesh/whole_logo.dae"
					marker.action = marker.ADD
					marker.scale.x = 0.25
					marker.scale.y = 0.25
					marker.scale.z = 0.25
					marker.pose.position.x = 0.5
					marker.pose.position.y = 5
					marker.pose.position.z = 0.5
					marker.pose.orientation.x = 0
					marker.pose.orientation.y = 0
					marker.pose.orientation.z = 0
					marker.pose.orientation.w = 0
					marker.mesh_use_embedded_materials = True
					mesh_pub.publish(marker)

					trans_p = [0, 0, 0]
					rot = tf.transformations.quaternion_from_euler(1.5707, 0, 1.5707) # transform the grasp poses to the robot (panda in this case) gripper configuration
					#print rot

					pose44 = np.dot(tf.transformations.translation_matrix(pose[0:3]), tf.transformations.quaternion_matrix(pose[3:8]))
					mat44 = np.dot(tf.transformations.translation_matrix(trans_p), tf.transformations.quaternion_matrix(rot))
					#txpose = np.dot(mat44, pose44)
					txpose = np.dot(pose44, mat44)
					pose = np.concatenate((tf.transformations.translation_from_matrix(txpose), tf.transformations.quaternion_from_matrix(txpose)), axis=0)

					nice_grasps = set(['1','2','6','7','9','15','17','19']) #to plot
					## Mesh accounting for 0.075m translation
					if str(idx) in nice_grasps:
						marker = Marker()
						marker.header.frame_id = 'panda_link0'
						marker.ns = world
						marker.type = marker.MESH_RESOURCE
						marker.id = idx+30
						marker.mesh_resource = "package://graspit_demo/models/mesh/panda_hand_m.dae"
						marker.action = marker.ADD
						marker.scale.x = 1.0
						marker.scale.y = 1.0
						marker.scale.z = 1.0
						marker.pose.position.x = pose[0]
						marker.pose.position.y = pose[1]
						marker.pose.position.z = pose[2]
						marker.pose.orientation.x = pose[3]
						marker.pose.orientation.y = pose[4]
						marker.pose.orientation.z = pose[5]
						marker.pose.orientation.w = pose[6]

						marker.mesh_use_embedded_materials = True

						mesh_pub.publish(marker)
						return pose

					print "============ Press `Enter` to see next grasp..."
					raw_input()


			#grasp_list = np.asarray(grasp_list)
			#ra.plotTrajectoryAsFrames(grasp_list, 'panda_link0', pub_list[i])

# if __name__ == "__main__":
#
# 	visualise_database()
