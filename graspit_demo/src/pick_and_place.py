#!/usr/bin/env python

import sys
import copy
import os
import rospy
import moveit_commander
import graspit_commander
import geometry_msgs.msg
import moveit_msgs.msg
from math import pi
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list
from tf.transformations import quaternion_from_euler

import pickle

from visualization_msgs.msg import Marker
from geometry_msgs.msg import PoseStamped, PoseArray

import visualise_database as vs

world_list = ["cylinder"]
cwd = os.getcwd()
store_data = cwd + '/data/'


def plan_grasps(gc):

	for world in world_list:
		pkl_file = str(store_data) + world + "_arbitrary_grasps.npy"
		with open(pkl_file,"a") as f:
			gc.clearWorld()
			gc.loadWorld(world)
			response = gc.planGrasps()
			grasps = response.grasps
			print grasps

			pickle.dump(grasps,f,pickle.HIGHEST_PROTOCOL)

	return grasps

def all_close(goal, actual, tolerance):
	"""
	Convenience method for testing if a list of values are within a tolerance of their counterparts in another list
	@param: goal       A list of floats, a Pose or a PoseStamped
	@param: actual     A list of floats, a Pose or a PoseStamped
	@param: tolerance  A float
	@returns: bool
	"""
	all_equal = True
	if type(goal) is list:
		for index in range(len(goal)):
		  if abs(actual[index] - goal[index]) > tolerance:
			  return False

	elif type(goal) is geometry_msgs.msg.PoseStamped:
		return all_close(goal.pose, actual.pose, tolerance)

	elif type(goal) is geometry_msgs.msg.Pose:
		return all_close(pose_to_list(goal), pose_to_list(actual), tolerance)

	return True

class MoveGroupPythonIntefaceTutorial(object):
	"""MoveGroupPythonIntefaceTutorial"""
	def __init__(self):
		super(MoveGroupPythonIntefaceTutorial, self).__init__()

		# Setting up moveit_commander and rospy
		moveit_commander.roscpp_initialize(sys.argv)
		rospy.init_node('pp_arbitrary_grasps_demo',
		anonymous=True)

		robot = moveit_commander.RobotCommander()
		scene = moveit_commander.PlanningSceneInterface()

		group_name = "panda_arm"
		group = moveit_commander.MoveGroupCommander(group_name)

		display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path',
		   moveit_msgs.msg.DisplayTrajectory,
		   queue_size=20)

		planning_frame = group.get_planning_frame()
		# print "============ Reference frame: %s" % planning_frame

		eef_link = group.get_end_effector_link()
		#print "============ End effector: %s" % eef_link

		# We can get a list of all the groups in the robot:
		group_names = robot.get_group_names()
		# print "============ Robot Groups:", robot.get_group_names()

		# Sometimes for debugging it is useful to print the entire state of the
		# robot:
		# print "============ Printing robot state"
		# print robot.get_current_state()
		# print ""

		# Misc variables
		self.object_name = ''
		self.robot = robot
		self.scene = scene
		self.group = group
		self.display_trajectory_publisher = display_trajectory_publisher
		self.planning_frame = planning_frame
		self.eef_link = eef_link
		self.group_names = group_names

	def go_to_joint_state(self):

		group = self.group

		joint_goal = group.get_current_joint_values()
		joint_goal[0] = 0
		joint_goal[1] = -pi/4
		joint_goal[2] = 0
		joint_goal[3] = -pi/2
		joint_goal[4] = 0
		joint_goal[5] = pi/3
		joint_goal[6] = 0

		group.go(joint_goal, wait=True)

		# Calling ``stop()`` ensures that there is no residual movement
		group.stop()

		current_joints = self.group.get_current_joint_values()
		return all_close(joint_goal, current_joints, 0.01)

	def go_to_pose_goal(self, grasp):

		group = self.group

		pose_goal = geometry_msgs.msg.Pose()
		#pose_goal = copy.deepcopy(grasp)

		pose_goal.position.x = grasp[0]
		pose_goal.position.y = grasp[1]-0.1
		pose_goal.position.z = grasp[2]

		#q = quaternion_from_euler(1.571, 0.785, 1.571)
		#grasp_frame_transform: [0, 0, 0.1, 1.571, 0.785, 1.571]
		pose_goal.orientation.x = grasp[3]
		pose_goal.orientation.y = grasp[4]
		pose_goal.orientation.z = grasp[5]
		pose_goal.orientation.w = grasp[6]

		group.set_pose_target(pose_goal)

		## Now, we call the planner to compute the plan and execute it.
		plan = group.go(wait=True)
		## Calling `stop()` ensures that there is no residual movement
		group.stop()
		group.clear_pose_targets()

		current_pose = self.group.get_current_pose().pose
		return all_close(pose_goal, current_pose, 0.01)

	def up(self):

		group = self.group

		poseStamped = group.get_current_pose()
		pose = poseStamped.pose
		target = pose
		target.position.z += .1
		plan_success, traj, planning_time, error_code = group.plan(target)
		group.execute(traj, wait=True)


	def down(self):

		group = self.group
		poseStamped = group.get_current_pose()
		pose = poseStamped.pose
		target = pose
		target.position.z -= .1
		print target
		plan_success, traj, planning_time, error_code = group.plan(target)
		group.execute(traj,wait=True)


	def wait_for_state_update(self, object_is_known=False, object_is_attached=False, timeout=4):
		object_name = self.object_name
		scene = self.scene

		start = rospy.get_time()
		seconds = rospy.get_time()
		while (seconds - start < timeout) and not rospy.is_shutdown():
		  # Test if the object is in attached objects
		  attached_objects = scene.get_attached_objects([object_name])
		  is_attached = len(attached_objects.keys()) > 0

		  # Test if the object is in the scene.
		  # Note that attaching the object will remove it from known_objects
		  is_known = object_name in scene.get_known_object_names()

		  # Test if we are in the expected state
		  if (object_is_attached == is_attached) and (object_is_known == is_known):
			  return True

		  # Sleep so that we give other threads time on the processor
		  rospy.sleep(0.1)
		  seconds = rospy.get_time()

		# If we exited the while loop without returning then we timed out
		return False

	def add_object(self, timeout=4):
		object_name = self.object_name
		scene = self.scene

		object_pose = geometry_msgs.msg.PoseStamped()
		object_pose.header.frame_id = "panda_link0"
		object_pose.pose.position.x = 0.5
		object_pose.pose.position.y = -0.25
		object_pose.pose.position.z = 0.15
		object_pose.pose.orientation.w = 1.0
		object_name = "cylinder"
		# scene.add_object(object_name, object_pose, size=(0.1, 0.1, 0.1))
		#file_name = '/home/pardon/ws_grasp_baselines/src/arbitrary_grasps/graspit_demo/models/mesh/cup.dae'
		# scene.add_mesh(object_name, object_pose, file_name,size=(0.1, 0.1, 0.1))
		scene.add_cylinder(object_name, object_pose, 0.25, 0.02) # 0.25 m of height and 0.02m of radius

		table_pose = geometry_msgs.msg.PoseStamped()
		table_pose.header.frame_id = "panda_link0"
		table_pose.pose.position.x = 0.5
		table_pose.pose.position.y = -0.25
		table_pose.pose.position.z = 0
		table_pose.pose.orientation.w = 1.0
		table_name = "table"
		scene.add_box(table_name, table_pose, size=(0.4, 0.5, 0.1))


		self.object_name=object_name
		self.table_name=table_name
		return object_pose, self.wait_for_state_update(object_is_known=True, timeout=timeout)


	def attach_object(self, timeout=4):
		object_name = self.object_name
		robot = self.robot
		scene = self.scene
		eef_link = self.eef_link
		group_names = self.group_names

		grasping_group = 'hand'
		touch_links = robot.get_link_names(group=grasping_group)
		# scene.attach_box(eef_link, object_name, touch_links=touch_links)
		scene.attach_mesh(eef_link, object_name, touch_links=touch_links)

		# We wait for the planning scene to update.
		return self.wait_for_state_update(object_is_attached=True, object_is_known=False, timeout=timeout)

	def detach_object(self, timeout=4):

		object_name = self.object_name
		scene = self.scene
		eef_link = self.eef_link

		scene.remove_attached_object(eef_link, name=object_name)

		# We wait for the planning scene to update.
		return self.wait_for_state_update(object_is_known=True, object_is_attached=False, timeout=timeout)

	def remove_object(self, timeout=4):
		object_name = self.object_name
		scene = self.scene
		scene.remove_world_object(object_name)
		# We wait for the planning scene to update.
		return self.wait_for_state_update(object_is_attached=False, object_is_known=False, timeout=timeout)


if __name__ == '__main__':

	sim_grasp = MoveGroupPythonIntefaceTutorial()

	print "============ Press `Enter` to begin pick-and-place task (press ctrl-d to exit)..."
	raw_input()
	object_pose, state = sim_grasp.add_object()

	# print "============ Press `Enter` stop used for plotting"
	# raw_input()
	# gc = graspit_commander.GraspitCommander()
	# grasps = plan_grasps(gc)

	grasp = vs.visualise_database()


	# for idx,grasp in enumerate(grasps):
	#
	# 	x = raw_input("1 for proceed with the last grasp, 0 to try another one\n")
	# 	if x != '1':
	# 		continue
	sim_grasp.go_to_joint_state
	sim_grasp.go_to_pose_goal(grasp)
	sim_grasp.attach_object()
	sim_grasp.up()
	sim_grasp.down()
	sim_grasp.detach_object()
		# sim_grasp.remove_object()
		# print "============ Press `Enter` to begin pick-and-place task (press ctrl-d to exit)..."
		# raw_input()
		# object_pose, state = sim_grasp.add_object()
