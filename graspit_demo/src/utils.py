import rospy
import time
import moveit_commander
import roslib; roslib.load_manifest('robotiq_2f_gripper_control')
from robotiq_2f_gripper_control.msg import _Robotiq2FGripper_robot_output  as outputMsg



def open_grip(pub):
	msg = outputMsg.Robotiq2FGripper_robot_output()
	msg.rACT = 1
	msg.rGTO = 1
	msg.rATR = 0
	msg.rPR  = 0
	msg.rSP = 255
	msg.rFR = 150
	pub.publish(msg)
	time.sleep(4)

def close_grip(pub):
	msg = outputMsg.Robotiq2FGripper_robot_output()
	msg.rACT = 1
	msg.rGTO = 1
	msg.rATR = 0
	msg.rPR  = 255
	msg.rSP = 255
	msg.rFR = 150
	pub.publish(msg)
	time.sleep(4)

def home_arm():
	mgc = moveit_commander.MoveGroupCommander("manipulator")
	mgc.get_current_joint_values()
	jv = mgc.get_current_joint_values()
	jv[0]=1.0

	mgc.set_joint_value_target(jv)
	mgc.plan()
	p = mgc.plan()
	mgc.execute(p)


def execute_traj(robot):
	group = robot.get_group('manipulator')
	poseStamped = group.get_current_pose()
	arm_pose = poseStamped.pose

	#offsets
	arm_pose.position.x = -.5
	arm_pose.position.y = 0.
	arm_pose.position.z = .5
	# arm_pose.orientation.x = 0
	# arm_pose.orientation.y = 0
	# arm_pose.orientation.z = 0
	arm_pose.orientation.w = 1
	print "arm_pose.pose: "
	print arm_pose

	traj = group.plan(arm_pose)
	t = 50

	for t in range(t):
		x = raw_input("1 for proceed with trajectory, 0 to generate a new one \n")
		print(x)
		if x != '1':
			traj = group.plan(arm_pose)
			continue
		group.execute(traj[1])
